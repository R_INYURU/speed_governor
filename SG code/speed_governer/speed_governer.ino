#include <Arduino.h>

/*
 * for creating a file for each day always when the device start it must check if 
 * there is a file having a name of today if not create an write in it.
 * 
*/

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <DNSServer.h>
#include <pgmspace.h>
#include <RtcDS1307.h>
#include <Wire.h>
#include <SD.h>
#include <SPI.h>
#include <EEPROM.h>

RtcDS1307<TwoWire> Rtc(Wire);
File myData;
ESP8266WebServer server(80);
IPAddress apIP(192, 168, 4, 1);
IPAddress netMsk(255, 255, 255, 0);
DNSServer dnsServer;

const byte DNS_PORT = 53;

const char* ssid = "Beno cars";
//const char* pswd = "123456789";

unsigned long elapsedTm = millis();
const uint8_t freq = D3;  //input pin
const uint8_t CS = D4;    //SD chip select pin
const uint8_t relay = D8; //output pin
const uint8_t addr = 21;
uint8_t read_serial;
uint8_t car_freq;
String Time, Date;
static bool hasSD = false;

void setup() {
  Serial.begin(115200);
  EEPROM.begin(512);
  Rtc.Begin();
  startAP_STA();

  if (EEPROM.read(addr) == 0) {
    EEPROM.write(addr, 1);
    EEPROM.commit(); //this line is for esp8266 modules.
  }
  Serial.println("EEPROM ok");
  if (!Rtc.IsDateTimeValid()) {
    Serial.println("RTC lost confidence in the DateTime!");
  }
  if (!Rtc.GetIsRunning()) {
    Serial.println("RTC was not actively running, starting now");
    Rtc.SetIsRunning(true);
  }
  if (!SD.begin(CS)) {  //start SD card
    Serial.println("SD Card has a problem");
    while (1); // STOP!!
  }
  Serial.println("SD Card ready.");
  if (SD.exists("index.htm")) {  //check if frequency log file exists
    Serial.println("index.htm exists.");
  }
  else {  //if it doesn't exists create it.
    Serial.println("index.htm doesn't exist.");
    Serial.println("Now file created.");
  }
  myData = SD.open("index.htm", FILE_WRITE);
  myData.close();

  attachInterrupt(freq, up, FALLING);
  pinMode(relay, OUTPUT);

  server.onNotFound(pageNotFound);
  server.begin();

  hasSD = true;
}

void loop() {
  server.handleClient();
  RtcDateTime now = Rtc.GetDateTime();

  processTime(now);
  if (Serial.available()) {
    read_serial = Serial.parseInt();
    EEPROM.write(addr, read_serial);
    EEPROM.commit(); //this line is for esp8266 modules.
    Serial.println(EEPROM.read(addr));
  }
  if ((elapsedTm - millis()) > 10000) {
    String freq_record;
    uint8_t Speed = calcSpeed(car_freq);
    if (Speed > 60) {
      freq_record += "<tr><td style=\"color:red\">";
    }
    else {
      freq_record += "<tr><td>";
    }
    freq_record += String(Speed) + "</td>";
    freq_record += "<td>" + Time + "</td><td>" + Date + "</td></tr>";
    sdWrite(freq_record);
    Serial.println("stored");
  }
  check_freq();
  Serial.println(String(car_freq) + " , " + Time + " , " + Date);
  car_freq = 0;
  delay(2000);
}

uint8_t calcSpeed(uint8_t curFreq) {
  uint8_t SGfreq = EEPROM.read(addr);
  uint8_t SGspeed = 60;

  return (curFreq * SGspeed) / SGfreq;
}

void sdWrite(String data_log) {
  myData = SD.open("index.htm", FILE_WRITE);
  if (myData) {
    Serial.println(data_log);
    myData.println(data_log);
  }
  else {
    Serial.println("Not possible to write on this SD card");
    Serial.println(data_log);
  }
  myData.close();
}

#define countof(a) (sizeof(a) / sizeof(a[0]))
void processTime(const RtcDateTime& dt) {
  char datestring[11];
  char timestring[9];
  snprintf_P(datestring, countof(datestring), PSTR("%02u/%02u/%04u"),
             dt.Month(), dt.Day(), dt.Year());
  snprintf_P(timestring, countof(timestring), PSTR("%02u:%02u:%02u"),
             dt.Hour(), dt.Minute(), dt.Second());
  Serial.println(datestring);
  Serial.println(timestring);
  Time = timestring;
  Date = datestring;
}

void check_freq() {
  if (car_freq > EEPROM.read(addr)) {
    digitalWrite(relay, HIGH);
  }
  else {
    digitalWrite(relay, LOW);
  }
}

void up() { // Interrupt service routine for interrupt 0
  car_freq++;
}

